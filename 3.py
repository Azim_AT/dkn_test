def minSwaps(arr):
    temp = arr.copy()
    temp.sort()
    positions = {}

    for i in range(len(arr)):
        positions[temp[i]] = i + 1

    count_swap = 0
    for i in range(len(arr)):
        if (i + 1) != positions[arr[i]]:
            temp = arr[i]
            pos = positions[arr[i] - 1]
            arr[i] = arr[pos]
            arr[pos] = temp
            count_swap += 1
    return count_swap

print(minSwaps([6,5,4,2,3,1,7]))